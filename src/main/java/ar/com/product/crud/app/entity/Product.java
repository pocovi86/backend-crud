package ar.com.product.crud.app.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //con esto digo que es autoincremental el id
    private int id;
    private String name;
    private float price;

    public Product() {
    }

    public Product(String name, Float price) {
        this.name = name;
        this.price = price;
    }
}
